const spawn = require('child_process').spawn;

/**
 * Represents the live stream manager.
 * This is responsible for playing back
 * from the RTMP stream for The Nations.
 *
 * @author heyitsleo
 * @type {StreamManager}
 */
module.exports = class StreamManager {
    /**
     * Set up the stream manager and declare fields.
     */
    constructor() {
        this.ffmpegProcess = null;
        this.voiceConnection = null;
        this.streamDispatcher = null;
    }

    /**
     * Begins playing the livestream to a voice connection.
     *
     * @param {VoiceConnection} connection The voice connection that we have.
     * @param {String} key that key tho
     */
    start(connection, key) {
        this.voiceConnection = connection;

        const process = spawn('ffmpeg', [
            '-i', 'rtmp://localhost/live/' + key,
            '-f', 'mp3',
            '-stimeout', Number.MAX_SAFE_INTEGER,
            'pipe:1'
        ]);

        process.stdout.on('data', () => {});
        process.stderr.on('data', () => {});

        this.ffmpegProcess = process;
        this.streamDispatcher = this.voiceConnection.playStream(this.ffmpegProcess.stdout, { volume: 2.5 });
    }

    /**
     * Restarts the playback of the livestream.
     */
    restart() {
        this.stop();
        this.start(this.voiceConnection);
    }

    /**
     * Stops the playback of the livestream.
     */
    stop() {
        if (this.ffmpegProcess) {
            this.ffmpegProcess.kill('SIGINT');

            this.ffmpegProcess = null;
        }

        this.streamDispatcher.end('BotStop');
    }

    /**
     * Begins playing the livestream to a voice connection, OR restarts the playback.
     *
     * @param {VoiceConnection} connection The voice connection that we have.
     */
    startOrRestart(connection) {
        if (this.ffmpegProcess)
            this.restart();
        else
            this.start(connection);
    }

    // ---------------------------- Getters

    /**
     * Gets the FFmpeg process, if there is one.
     *
     * @returns {null|ChildProcess}
     */
    getFFmpegProcess() {
        return this.ffmpegProcess;
    }

    /**
     * Gets the StreamDispatcher, if there is one.
     *
     * @returns {null|StreamDispatcher}
     */
    getStreamDispatcher() {
        return this.streamDispatcher;
    }

    /**
     * Gets the VoiceConnection, if there is one.
     *
     * @returns {null|VoiceConnection}
     */
    getVoiceConnection() {
        return this.voiceConnection;
    }
};