const rtmpdump = require('rtmpdump');

module.exports = class StreamChecker {
    check(token) {
        return new Promise((resolve, reject) => {
            const stream = rtmpdump.createStream({
                rtmp: 'rtmp://localhost/live/' + token
            });

            stream.on('connected', (info) => resolve(info));

            stream.on('error', (err) => reject(err));
        });
    }
};