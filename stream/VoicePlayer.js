let instances = require('../instances');

module.exports = {
    play(token, voiceChannel, msg, targetMember, checker) {
        return voiceChannel.join().then(connection => {
            return checker.check(token).then(() => {
                instances.streamManager.start(connection, token);
                instances.currentDj = targetMember.user.id;

                return msg.reply(`:headphones: Started!`);
            }).catch((error) => {
                connection.disconnect();

                if (error) {
                    let message = error.message;

                    if (message) {
                        if (message.includes('Connection refused')) {
                            return msg.reply(`\uD83D\uDE41 The stream seems to be down, unfortunately.`);
                        } else if (message.includes('rtmp server sent error')) {
                            return msg.reply(`:x: **${targetMember.user.tag}** is not streaming!`);
                        } else {
                            return msg.reply(`:x: Something went wrong: **${message}**`);
                        }
                    } else {
                        return msg.reply(`:x: Got an error with no message :thinking:`);
                    }
                } else {
                    return msg.reply(`:x: Error-ception? :thinking: Apparently we got an error, but there is no error?`);
                }
            });
        }).catch((err) => {
            console.error(err);
            return msg.reply(`:x: Something went wrong. Not sure how this even happened.`);
        });
    }
};