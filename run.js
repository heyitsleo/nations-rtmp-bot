const Bot = require('./bot');
const config = require('./config.json');
const instances = require('./instances');

if (config.streamInfo) {
    instances.streamInfo.streamKey = config.streamInfo.streamKey;
    instances.streamInfo.url = config.streamInfo.url;
}

new Bot(config).start();