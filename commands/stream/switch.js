const commando = require('discord.js-commando');
const chalk = require('chalk');
const StreamChecker = require('../../stream/StreamChecker');
const TokenProvider = require('../../database/TokenProvider');
const VoicePlayer = require('../../stream/VoicePlayer');
let instances = require('../../instances');

module.exports = class SwitchCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'switch',
            aliases: ['switch-stream', 'switch-rtmp'],
            group: 'stream',
            memberName: 'switch',
            description: 'Switches the user whose stream should be played.',
            guildOnly: true,

            args: [
                {
                    key: 'member',
                    label: 'member',
                    prompt: 'Whose stream would you like to play?',
                    type: 'member'
                }
            ]
        });

        this.checker = new StreamChecker();
        this.tokenProvider = new TokenProvider();
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    /**
     * @param msg
     * @param {Object} args
     */
    async run(msg, args) {
        let guild = msg.guild;
        let voiceChannel;
        let targetMember = args.member;

        if (!guild.voiceConnection) {
            return msg.reply(`:headphones: I'm not playing the livestream! Did you mean to use \`start [mention]\`?`);
        }

        if (instances.currentDj === targetMember.user.id) {
            return msg.reply(`:headphones: This user (**${targetMember.user.tag}**) is already having their stream played!`);
        }

        voiceChannel = guild.voiceConnection.channel;

        guild.voiceConnection.channel.leave();

        return this.tokenProvider.getToken(targetMember.user.id)
            .then(tokenObject => VoicePlayer.play(tokenObject.token, voiceChannel, msg, targetMember, this.checker))
            .catch(err => {
                console.error(err);
                return msg.reply(`:x: Something went wrong while fetching data. Not sure how this even happened.`);
            });
    }
};