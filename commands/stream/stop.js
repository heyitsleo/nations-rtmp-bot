const commando = require('discord.js-commando');
const instances = require('../../instances');
const chalk = require('chalk');

module.exports = class StopCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'stop',
            aliases: ['stop-stream', 'stop-rtmp'],
            group: 'stream',
            memberName: 'stop',
            description: 'Stops playing back the live stream.',
            guildOnly: true
        });
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    async run(msg, args) {
        let guild = msg.guild;

        if (!guild.voiceConnection) {
            return msg.reply(`:headphones: I'm not playing the livestream!`);
        }

        guild.voiceConnection.disconnect();
        instances.streamManager.stop();
        instances.currentDj = null;

        return msg.reply(`\uD83D\uDD07 Stopped!`);
    }
};