const instances = require('../../instances');
const chalk = require('chalk');
const commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine, stripIndents = require('common-tags').stripIndents;

module.exports = class LiveCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'live',
            group: 'stream',
            memberName: 'live',
            description: 'Sends a "live" announcement to the given channel..',
            guildOnly: true,
            args: [
                {
                    key: 'channel',
                    label: 'textchannel',
                    prompt: 'What channel should the announcement be sent to?',
                    type: 'channel'
                }
            ]
        });
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    /**
     * @param msg
     * @param {Object} args
     */
    async run(msg, args) {
        let guild = msg.guild;
        let vc;

        if (!(vc = guild.voiceConnection)) {
            return msg.reply(`:headphones: I'm not playing the livestream!`);
        }

        if (!instances.currentDj) {
            return msg.reply(`:x: There is no current DJ.`);
        }

        let member = guild.member(instances.currentDj);

        if (!member) {
            return msg.reply(`:x: Couldn't find the DJ?!`);
        }

        return args.channel.send(stripIndents`
            :headphones: **${member.user.tag}** is now playing live music! Join the **${vc.channel.name}** channel to hear it!
        `);
        // return guild.fetchMember(instances.currentDj)
        //     .then(member => {
        //         return args.channel.send(stripIndents`
        //             :headphones: **${member.user.tag}** is now playing live music! Join the **${vc.channel.name}** channel to hear it!
        //         `);
        //     })
        //     .catch(err => {
        //         return msg.reply(`:x: Couldn't find the DJ?!`);
        //     });
    }
};