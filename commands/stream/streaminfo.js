const commando = require('discord.js-commando');
const instances = require('../../instances');
const chalk = require('chalk');
const stripIndents = require('common-tags').stripIndents;
const TokenProvider = require('../../database/TokenProvider');
const ReactionCollector = require('discord.js').ReactionCollector;

module.exports = class StreamInfoCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'streaminfo',
            group: 'stream',
            memberName: 'streaminfo',
            description: 'Gets the stream info you need to use for OBS.',
            guildOnly: true
        });

        this.tokenProvider = new TokenProvider();
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    async run(msg, args) {
        let streamInfo = instances.streamInfo;

        if (!streamInfo.url) {
            return msg.reply(`:x: I can't give you the stream information, as it is not complete.`);
        }

        return this.tokenProvider.getToken(msg.author.id)
            .then(tokenObject => {
                return msg.direct(stripIndents`
                    **Stream Type**: **Custom**
                    **Stream URL**: ${streamInfo.url}
                    **Stream Key**: ${tokenObject.token}
                `).then(() => {
                    return msg.reply(`Sent you the stream info in DMs.`);
                }).catch(() => {
                    return msg.reply(`Couldn't send you the stream info in DMs.`);
                });
            }).catch(() => {
                return msg.reply(`Failed to retrieve your stream token.`);
            });
    }
};