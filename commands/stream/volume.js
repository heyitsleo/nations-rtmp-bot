const instances = require('../../instances');
const chalk = require('chalk');
const commando = require('discord.js-commando');

module.exports = class VolumeCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'volume',
            aliases: ['vol'],
            group: 'stream',
            memberName: 'volume',
            description: 'Changes the stream playback volume.',
            guildOnly: true,

            args: [
                {
                    key: 'volume',
                    prompt: 'What is the volume you would like to set? (1-100)',
                    type: 'float',
                    'default': -1,
                    min: 1,
                    max: 100
                }
            ]
        });
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    async run(msg, args) {
        let guild = msg.guild, volume = args['volume'];

        if (!guild.voiceConnection) {
            return msg.reply(`:headphones: I'm not playing the livestream!`);
        }

        if (volume !== -1) {
            instances.streamManager.getStreamDispatcher().setVolume(volume / 100);

            return msg.reply(`\u23EF Changed volume to **${volume}%**!`);
        } else {
            return msg.reply(`\u23EF Current Volume: **${instances.streamManager.getStreamDispatcher().volume * 100}%**`);
        }
    }
};