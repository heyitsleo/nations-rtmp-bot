const instances = require('../../instances');
const chalk = require('chalk');
const commando = require('discord.js-commando');

module.exports = class PauseCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'pause',
            group: 'stream',
            memberName: 'pause',
            description: 'Pauses the stream playback.',
            guildOnly: true
        });
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    async run(msg, args) {
        let guild = msg.guild;

        if (!guild.voiceConnection) {
            return msg.reply(`:headphones: I'm not playing the livestream!`);
        }

        instances.streamManager.getStreamDispatcher().pause();

        return msg.reply(`\u23F8 Paused!`);
    }
};