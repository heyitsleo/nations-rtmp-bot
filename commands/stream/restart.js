const commando = require('discord.js-commando');
const instances = require('../../instances');
const chalk = require('chalk');

module.exports = class RestartCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'restart',
            group: 'stream',
            memberName: 'restart',
            description: 'Restarts the stream playback.',
            guildOnly: true
        });
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    async run(msg, args) {
        let guild = msg.guild;

        if (!guild.voiceConnection) {
            return msg.reply(`:headphones: I'm not playing the livestream!`);
        }

        instances.streamManager.restart();

        return msg.reply(`\uD83D\uDD01 Restarted!`);
    }
};