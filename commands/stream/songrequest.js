/**
 * @type {YoutubeSearcher}
 */
const instances = require('../../instances');
const chalk = require('chalk');
const commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine, stripIndents = require('common-tags').stripIndents;

const ReactionCollector = require("discord.js").ReactionCollector;
const youtubeSearcher = instances['youtubeSearcher'];

module.exports = class SongRequestCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'songrequest',
            group: 'stream',
            memberName: 'songrequest',
            description: 'Sends the current DJ a song request.',
            guildOnly: true,
            args: [
                {
                    type: 'string',
                    infinite: true,
                    min: 1,
                    max: 250,
                    prompt: 'YouTube search?',
                    label: 'search',
                    key: 'search'
                }
            ]
        });
    }

    /**
     * @param msg
     * @param {Object} args
     */
    async run(msg, args) {
        let guild = msg.guild;

        if (!youtubeSearcher) {
            return msg.reply(`:x: YouTube searching is disabled.`);
        }

        if (!guild.voiceConnection) {
            return msg.reply(`:headphones: I'm not playing the livestream!`);
        }

        if (!instances.currentDj) {
            return msg.reply(`:x: There is no DJ to send the song request to.`);
        }

        return youtubeSearcher.search(args.search.join(' '))
            .then(result => {
                if (typeof result === 'undefined') {
                    return msg.reply(`:x: Couldn't find any results.`);
                }

                let member = guild.member(instances.currentDj);

                if (!member) {
                    return msg.reply(`:x: Couldn't find the DJ?!`);
                }

                return member.user.send(stripIndents`
                    Hey! **${msg.author.tag}** has requested that you play the song **${result['snippet']['title']}** 
                    (https://youtube.com/watch?v=${result['id']['videoId']})
                            
                    Feel free to ignore this if you don't plan to accept requests.
                    React with :white_check_mark: to accept the request, or :x: to deny.
                `).then((m) => {
                    let collector = new ReactionCollector(
                        m,
                        (reaction) => reaction.emoji.name === '✅' || reaction.emoji.name === '❌',
                        {max: 1}
                    );

                    collector.on('collect', (reaction) => {
                        if (reaction.emoji.name === '✅') {
                            // accepted
                            msg.channel.sendMessage(`${msg.member.user} Your request was accepted!`).then();
                        } else {
                            msg.channel.sendMessage(`${msg.member.user} Your request was denied.`).then();
                        }
                    });

                    return msg.reply(`:white_check_mark: Song request sent!`);
                }).catch(() => {
                    return msg.reply(`:x: DJ isn't accepting DMs :(`);
                });
            })
            .catch(error => {
                console.error(error);
                return msg.reply(`:x: Couldn't search YouTube, sorry!`);
            });
    }
};