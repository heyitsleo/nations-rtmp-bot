const commando = require('discord.js-commando');
const instances = require('../../instances');
const chalk = require('chalk');

module.exports = class ResumeCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'resume',
            group: 'stream',
            memberName: 'resume',
            description: 'Resumes the stream playback.',
            guildOnly: true
        });
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    async run(msg, args) {
        let guild = msg.guild;

        if (!guild.voiceConnection) {
            return msg.reply(`:headphones: I'm not playing the livestream!`);
        }

        instances.streamManager.getStreamDispatcher().resume();

        return msg.reply(`\u23EF Resumed!`);
    }
};