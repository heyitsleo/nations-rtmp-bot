const commando = require('discord.js-commando');
const chalk = require('chalk');
const StreamChecker = require('../../stream/StreamChecker');
const TokenProvider = require('../../database/TokenProvider');
const VoicePlayer = require('../../stream/VoicePlayer');

module.exports = class StartCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'start',
            aliases: ['start-stream', 'start-rtmp'],
            group: 'stream',
            memberName: 'start',
            description: 'Begins playing back the live stream.',
            guildOnly: true,

            args: [
                {
                    key: 'member',
                    label: 'member',
                    prompt: 'Whose stream would you like to play?',
                    type: 'member'
                }
            ]
        });

        this.checker = new StreamChecker();
        this.tokenProvider = new TokenProvider();
    }

    hasPermission(message) {
        return message.member.roles.exists('name', 'Bouncer')
            || global.config.ownerId === message.author.id;
    }

    /**
     * @param msg
     * @param {Object} args
     */
    async run(msg, args) {
        let guild = msg.guild;
        let voiceChannel;
        let targetMember = args.member;

        if (guild.voiceConnection) {
            return msg.reply(`:headphones: I'm already playing the livestream in **${guild.voiceConnection.channel.name}**!`);
        }

        if (!(voiceChannel = msg.member.voiceChannel)) {
            return msg.reply(`:x: Hmm, maybe you should join a voice channel first?`);
        }

        if (!voiceChannel.joinable) {
            return msg.reply(`:x: I can't join **${voiceChannel.name}**.`);
        }

        return this.tokenProvider.getToken(targetMember.user.id)
            .then(tokenObject => VoicePlayer.play(tokenObject.token, voiceChannel, msg, targetMember, this.checker))
            .catch(err => {
                console.error(err);
                return msg.reply(`:x: Something went wrong while fetching data. Not sure how this even happened.`);
            });
    }
};