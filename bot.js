/**
 * The main bot class.
 *
 * @author heyitsleo
 */

const Commando = require('discord.js-commando'),
    path = require('path'),
    dbConnector = require('./database/connector'),
    instances = require('./instances'),
    YoutubeSearcher = require('./util/YoutubeSearcher'),
    chalk = require('chalk');

module.exports = class Bot {
    constructor({token, ownerId = '196245583445491712', prefix = '&', googleApiKey = ''} = {}) {
        this.token = token;
        this.ownerId = ownerId;
        this.prefix = prefix;
        this.googleApiKey = googleApiKey;

        global['config'] = {
            token,
            ownerId,
            prefix,
            googleApiKey
        };
    }

    start() {
        if (this.googleApiKey) {
            instances.youtubeSearcher = new YoutubeSearcher(this.googleApiKey);
        }

        console.log(`${chalk.yellow('Database>')} ${chalk.green('Connecting...')}`);

        dbConnector.connect().then(connection => {
            instances.dbConnection = connection;

            console.log(`${chalk.yellow('Database>')} ${chalk.green(`Connected!`)}`);
            console.log(`${chalk.yellow('Bot>')} ${chalk.green(`Starting...`)}`);

            const client = new Commando.Client({
                owner: this.ownerId,
                commandPrefix: this.prefix,
                unknownCommandResponse: false
            });

            client
                .on('error', error => console.log(`${chalk.red(error)}`))
                .on('warn', console.warn)
                .on('ready', () => {
                    console.log(`${chalk.yellow('Bot>')} ${chalk.green(`Logged in as ${client.user.tag}`)}`);
                });

            client.registry
                .registerGroup('stream', 'Stream')
                .registerDefaults()
                .registerCommandsIn(path.join(__dirname, 'commands'));
            return client.login(this.token);
        });
    }
};