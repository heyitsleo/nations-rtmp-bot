const YouTube = require('youtube-node');
const assert = require('assert');

module.exports = class YoutubeSearcher {
    constructor(key) {
        assert.notEqual(key, null, 'No key provided');

        this.key = key;
        this.client = new YouTube();
        this.client.setKey(this.key);
    }

    search(query) {
        return new Promise((resolve, reject) => {
            this.client.addParam('type', 'video');

            this.client.search(query, 1, (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results.items[0]);
                }
            });
        });
    }
};