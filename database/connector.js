const mongo = require('mongodb'),
    MongoClient = mongo.MongoClient,
    log = require('fancy-log');

let dbApi = {};
let url = "mongodb://localhost:27017/nations-dj-bot";

dbApi.connect = () => {
    return new Promise((resolve, reject) => {
        return MongoClient.connect(url, (err, db) => {
            if (err) {
                log.error(`Couldn't connect to database!`);

                reject(err);
            } else {
                log.info(`Established DB connection.`);

                resolve(db);
            }
        });
    });
};

/**
 * Inserts data into a collection
 *
 * @param db {Db}
 * @param collectionName {String}
 * @param data {Object}
 */
dbApi.insert = (db, collectionName, data) => {
    return db.collection(collectionName).insertOne(data);
};

/**
 * Updates data in a collection
 *
 * @param db {Db}
 * @param collectionName {String}
 * @param criteria
 * @param data {Object}
 */
dbApi.update = (db, collectionName, criteria, data) => {
    return db.collection(collectionName).updateOne(criteria, data);
};

/**
 * Gets data from a collection
 *
 * @param db {Db}
 * @param collectionName {String}
 * @param query {Object}
 */
dbApi.get = (db, collectionName, query) => {
    return db.collection(collectionName).findOne(query);
};

dbApi.getAll = (db, collectionName) => {
    return db.collection(collectionName).find().toArray();
};

dbApi.count = (db, collectionName, cObject) => {
    return db.collection(collectionName).count(cObject);
};

dbApi.has = (db, collectionName, cObject) => {
    return typeof dbApi.get(db, cObject, collectionName) !== 'undefined';
};

module.exports = dbApi;