let instances = require('../instances');
let connector = require('../database/connector');

module.exports = class TokenProvider {
    constructor() {
        if (!instances.dbConnection) {
            throw 'No DB connection!';
        }
    }

    /**
     * Gets the token for a user.
     *
     * @param {String} userId The ID of the user to fetch a token for.
     * @returns {Promise} A Promise, because async
     */
    getToken(userId) {
        return connector.get(instances.dbConnection, 'tokens', {
            userId: userId
        }).then(result => {
            if (!result || typeof result === 'undefined') {
                return connector.insert(instances.dbConnection, 'tokens', {
                    userId: userId,
                    token: require('../util/strings').randomAsciiString(8)
                }).then(r => r.ops[0]);
            }

            return result;
        }).catch(console.error);
    }
};