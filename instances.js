const StreamManager = require('./stream/StreamManager');

module.exports = {
    streamManager: new StreamManager(),
    youtubeSearcher: null,
    dbConnection: null,
    streamInfo: {
        url: null,
        streamKey: null
    },
    currentDj: null
};